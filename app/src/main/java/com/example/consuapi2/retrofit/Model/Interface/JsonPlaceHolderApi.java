package com.example.consuapi2.retrofit.Model.Interface;

import com.example.consuapi2.retrofit.Model.Model.Posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

    @GET("posts")
    Call<List<Posts>> getPosts();
}

