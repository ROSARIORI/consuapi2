package com.example.consuapi2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.consuapi2.retrofit.Model.Interface.JsonPlaceHolderApi;
import com.example.consuapi2.retrofit.Model.Model.Posts;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView mJstonTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mJstonTxtView = findViewById(R.id.jsonText);
        getPosts();
    }


    private void getPosts(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi JsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<Posts>> call = JsonPlaceHolderApi.getPosts();
        call.enqueue(new Callback<List<Posts>>() {
            @Override
            public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {

                if (!response.isSuccessful()){
                    mJstonTxtView.setText("Codigo: "+response.code());
                    return;

                }

                List<Posts> PostList = response.body();

                for (Posts post: PostList){
                    String Content ="";
                    Content += "userId"+ post.getUserId() + "\n";
                    Content += "Id"+ post.getUserId() + "\n";
                    Content += "title"+ post.getTitle() + "\n";
                    Content += "body"+ post.getBody() + "\n";
                    mJstonTxtView.append(Content);
                }


            }

            @Override
            public void onFailure(Call<List<Posts>> call, Throwable t) {
                mJstonTxtView.setText(t.getMessage());

            }
        });


    }
}